"""
Este módulo se encarga de hacer operaciones matemáticas básicas.
"""
def suma(number_one, number_two):
    """Returns the sum of two numbers.

    Args:

        number_one(float)
        number_two(float)

    Returns:

        total = number_one + number_two
    """
    total = number_one + number_two
    return total
